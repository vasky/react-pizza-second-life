import axios from "axios";

export const fetchPizzas = (idCategory, {type:typeSort}) => (dispatch)=> {
    const directionSort = typeSort === 'rating' ? 'desc' : 'asc'
    dispatch(setLoadedAction(false))
    axios(`http://localhost:3001/pizzas?${
        idCategory !== 'null' ? 'category=' + idCategory : ''
    }${
        typeSort ? '&_sort=' + typeSort + '&_order=' + directionSort : ''
    }`).then(({data})=>{
        dispatch(pizzas(data))
    })
}

export const pizzas = (items) =>({
    type: 'SET_PIZZAS',
    payload: items
})

export const setLoadedAction = (loadedPizzaStatus) => ({
    type: 'SET_LOADED',
    payload: loadedPizzaStatus
})
