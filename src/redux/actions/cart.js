export const storeAction = (cart)=>({
    type: "ADD_PIZZA",
    payload: cart
})

export const clearCartAction = ()=>({
    type: "CLEAR_CART"
})
