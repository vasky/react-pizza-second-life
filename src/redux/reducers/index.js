import { combineReducers } from "redux";

import filters from './filters'
import {pizzas} from './pizzas'
import {cartReducer} from './cart'

const rootReducer = combineReducers({
    filters,
    pizzas,
    cartReducer
})
export default rootReducer
