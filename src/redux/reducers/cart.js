const initialState = {
    cart: {
        pizzas: {

        },
        amountPizzas: 0,
        totalPrice: 0
    }
}

export const cartReducer= (state = initialState, action)=> {
    switch(action.type){
        case 'ADD_PIZZA':
            return {
                ...state,
                cart: action.payload
            }
        case 'CLEAR_CART':
            return {
                cart: {
                    pizzas: {

                    },
                    amountPizzas: 0,
                    totalPrice: 0
                }
            }
        default:
            return state
    }
}
