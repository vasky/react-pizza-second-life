import './App.css';
import { Header } from "./components";
import {Route} from "react-router-dom";
import {Home, Cart} from './pages/'
import React from 'react'
import {  useSelector, useDispatch } from 'react-redux'
import { fetchPizzas } from "./redux/actions/pizzas";

function App() {

    const dispatch = useDispatch()
    const items = useSelector(({ pizzas }) => pizzas.items)
    const { category, sortBy } = useSelector(({filters})=>filters)

    React.useEffect(()=>{
        dispatch(fetchPizzas(category, sortBy))
    }, [category, sortBy])

    return(
        <div className="wrapper">
            <Header />
            <div className="content">
                <Route path='/' render={()=><Home items={items} />} exact/>
                <Route path='/cart' component={Cart}/>
            </div>
        </div>
    )
}

export default App;
