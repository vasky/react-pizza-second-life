import React from 'react'
import PropTypes from 'prop-types'

function Sort({ onClickSort }) {
    const [activeIndex, setActive] = React.useState(0)
    const setActiveFunc = (index) => {
        setActive(index)
        onClickSort({type:itemsSort[index].type})
    }

    const [showItems, setShowItems] = React.useState(false)
    const setShowItemsFunc = () => {
        setShowItems(!showItems)
    }
    const sortRef = React.useRef();

    React.useEffect(()=>{
        document.addEventListener("click", (e)=>{
            if(!e.path.includes(sortRef.current)){
                setShowItems(false)
            }
        })
    },
[])
    const itemsSort = [
        {
            name: 'популярности',
            type: "rating"
        },
        {
            name: 'цене',
            type: "price"
        },
        {
            name: 'алфавиту',
            type: "alphabet"
        }]

    return(
        <div className="sort" ref={(element)=> {
            sortRef.current = element
        }}>
            <div className="sort__label">
                <svg
                    width="10"
                    height="6"
                    viewBox="0 0 10 6"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    className={ !showItems ?'closeSortListIconArrow' : ''}
                >
                    <path
                        d="M10 5C10 5.16927 9.93815 5.31576 9.81445 5.43945C9.69075 5.56315 9.54427 5.625 9.375 5.625H0.625C0.455729 5.625 0.309245 5.56315 0.185547 5.43945C0.061849 5.31576 0 5.16927 0 5C0 4.83073 0.061849 4.68424 0.185547 4.56055L4.56055 0.185547C4.68424 0.061849 4.83073 0 5 0C5.16927 0 5.31576 0.061849 5.43945 0.185547L9.81445 4.56055C9.93815 4.68424 10 4.83073 10 5Z"
                        fill="#2C2C2C"
                    />
                </svg>
                <b>Сортировка по:</b>
                <span onClick={setShowItemsFunc}>{itemsSort[activeIndex].name}</span>
            </div>
            { showItems && <div className="sort__popup">
                <ul>
                    {itemsSort.map((sortItem, index) => {
                        return <li onClick={() => setActiveFunc(index)} key={sortItem.name + "__" + index}
                                   className={index === activeIndex ? 'active' : ""}>{sortItem.name}</li>
                    })}
                </ul>
            </div>}
        </div>
    )
}

Sort.propTypes = {
    items: PropTypes.arrayOf(PropTypes.string)
}
Sort.defaultProps = {
    items: []
}

export default React.memo( Sort );
