import React from 'react'
import ContentLoader from "react-content-loader";

const PizzaLoader = (props) => {
    return <ContentLoader
        speed={2}
        width={280}
        height={460}
        viewBox="0 0 280 460"
        backgroundColor="#e0e0e0"
        foregroundColor="#ffffff"
        {...props}
    >
        <circle cx="125" cy="125" r="125" />
        <rect x="1" y="308" rx="10" ry="10" width="268" height="86" />
        <rect x="0" y="270" rx="10" ry="10" width="268" height="24" />
        <rect x="147" y="411" rx="30" ry="30" width="132" height="46" />
        <rect x="2" y="419" rx="10" ry="10" width="95" height="36" />
    </ContentLoader>
}

export default PizzaLoader
