import React from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import {undefinedPizzaImage} from './../../assets/img/'
import { useDispatch, useSelector } from "react-redux";
import { storeAction } from "./../../redux/actions/cart";

const PizzaBlock = ({rating, name, imageUrl, price, types, sizes:sizesAvailable, id}) => {
    // "imageUrl", "category///3", "price//990", "types//[0,1]", "sizes[26,30,40]"
    const dispatch = useDispatch();
    const { cart } = useSelector(({cartReducer})=> cartReducer )
    const typesAvailable = ['тонкое', 'традиционное']
    const [activeType, setActiveType] = React.useState(types[0])
    const setActiveTypeFunc = (index) => {
        setActiveType(index)
    }

    const sizes = [ 26, 30, 40 ]
    const [activeSize, setActiveSize] = React.useState(sizesAvailable[0])
    const setActiveSizeFunc = (size) => {
        setActiveSize(size)
    }
    const amountInCart = cart.pizzas[id+name+typesAvailable[activeType]+activeSize] ?
        cart.pizzas[id+name+typesAvailable[activeType]+activeSize].amount : 0
    const onClickAddToCart = () => {
        let newCart = {
            ...cart,
            pizzas: {
                ...cart.pizzas,
                [id+name+typesAvailable[activeType]+activeSize]: {
                    pizzaId: id,
                    amount: cart.pizzas[id+name+typesAvailable[activeType]+activeSize] ? ++cart.pizzas[id+name+typesAvailable[activeType]+activeSize].amount : 1,
                    price: price,
                    size: activeSize,
                    type: typesAvailable[activeType],
                    img: imageUrl,
                    name: name
                }
            },
        }
        newCart = {
            ...newCart,
            amountPizzas: Object.keys(newCart.pizzas).reduce(( total, key ) => {
                return total += Number( newCart.pizzas[key].amount )
            }, 0),
            totalPrice: Object.keys(newCart.pizzas).reduce((total, key) => {
                return total += Number(newCart.pizzas[key].price * Number( newCart.pizzas[key].amount ))
            }, 0)
        }
        dispatch(storeAction(newCart))
    }
    return (
        <div className="pizza-block">
            <img
                className="pizza-block__image"
                src={imageUrl}
                alt="Pizza"
            />
            <h4 className="pizza-block__title">{name}</h4>
            <div className="pizza-block__selector">
                <ul>
                    { typesAvailable.map((type,index)=>{
                            return <li
                                key={type + '__' + index}
                                className={
                                    classNames({
                                        'active':activeType===index,
                                        'disabled': !types.includes(index),
                                    })} onClick={()=>setActiveTypeFunc(index)}>{type}</li>
                        }) }
                </ul>
                <ul>
                    { sizes.map((size, index)=> {
                        return <li
                            key={size}
                            onClick={()=>{setActiveSizeFunc(size)}}
                            className={
                                classNames({
                                    'active': activeSize === size,
                                    'disabled': !sizesAvailable.includes(size)
                                })
                            }>{size} см.</li>
                    }) }
                </ul>
            </div>
            <div className="pizza-block__bottom" >
                <div className="pizza-block__price">от {price} ₽</div>
                <div className={
                    classNames({
                        "button": true,
                        'button--add': true,
                        'button--outline': amountInCart ? 1 : 0
                    })
                } onClick={ ()=>onClickAddToCart() }>
                    <svg
                        width="12"
                        height="12"
                        viewBox="0 0 12 12"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M10.8 4.8H7.2V1.2C7.2 0.5373 6.6627 0 6 0C5.3373 0 4.8 0.5373 4.8 1.2V4.8H1.2C0.5373 4.8 0 5.3373 0 6C0 6.6627 0.5373 7.2 1.2 7.2H4.8V10.8C4.8 11.4627 5.3373 12 6 12C6.6627 12 7.2 11.4627 7.2 10.8V7.2H10.8C11.4627 7.2 12 6.6627 12 6C12 5.3373 11.4627 4.8 10.8 4.8Z"
                            fill="white"
                        />
                    </svg>
                    <span>Добавить</span>
                    {
                        amountInCart !== 0 ? <i>{amountInCart}</i> : false
                    }
                </div>
            </div>
        </div>
    )
}

PizzaBlock.propTypes = {
    name: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    types: PropTypes.arrayOf(PropTypes.number).isRequired,
    sizes: PropTypes.arrayOf(PropTypes.number).isRequired,
}
PizzaBlock.defaultProps = {
    name: 'недоступно',
    imageUrl: undefinedPizzaImage,
    price: "000",
    types: [],
    sizes: [],
}
export default React.memo( PizzaBlock )
