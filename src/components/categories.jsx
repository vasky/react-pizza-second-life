import React from 'react'
import PropTypes from 'prop-types'

function Category({categories, onClickCategory, activeCategory}) {
    return(
        <div className="categories">
            <ul>
                <li onClick={()=>onClickCategory("null")}
                    className={ activeCategory === "null" ? 'active' : ""}
                >все</li>

                {categories.map((category,index)=>{
                    return <li onClick={()=>onClickCategory(index)}
                               className={ index === activeCategory ? 'active' : ""}
                               key={category + "__" + index}>{category}</li>
                })}
            </ul>
        </div>
    )
}

Category.propTypes = {
    categories: PropTypes.arrayOf(PropTypes.string)
}
Category.defaultProps = {
    categories: ['']
}

export default Category
