import React from 'react'
import {Category, Sort} from "../components";
import { useSelector, useDispatch } from 'react-redux'
import {setCategory, sortBy} from "../redux/actions/filters";
import PizzaBlock from './../components/pizzaBlock/index'
import PizzaLoaderBlock from "../components/pizzaBlock/pizzaLoader";

const Home = ({items})=>{
    const dispatch = useDispatch()
    const { category: activeCategory, isLoaded} = useSelector(({filters, pizzas}) => {
        const obj = new Object(filters)
        obj.isLoaded = pizzas.isLoaded
        return obj
    } )
    const onClickCategory = (index) => {
        dispatch(setCategory(index))
    }
    const onClickSort = (type) => {
        dispatch(sortBy(type))
    }

    return (
        <div className="container">
            <div className="content__top">
                <Category
                    activeCategory = { activeCategory }
                    onClickCategory = { (index) => onClickCategory(index) }
                    categories={[ "Мясные", "Вегетарианская", "Гриль", "Острые", "Закрытые"]} />
                <Sort onClickSort={onClickSort} />
            </div>
            <h2 className="content__title">Все пиццы</h2>
            <div className="content__items">
                {isLoaded ? items.map(pizza => {
                    return <PizzaBlock key={pizza.id} {...pizza} id={pizza.id}/>
                }) :
                    Array(12).fill(<PizzaLoaderBlock/>)
                }
            </div>
        </div>
    )
}

export default Home
